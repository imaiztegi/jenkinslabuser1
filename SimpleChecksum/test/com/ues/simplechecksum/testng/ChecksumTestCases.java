package com.ues.simplechecksum.testng;

import org.testng.annotations.Test;

import com.ues.simplechecksum.Checksum;

import org.testng.AssertJUnit;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ChecksumTestCases {

	@Test
	public void testFile1() {
		//Create checksum for this file
		File file = new File("C:\\Users\\Administrator\\AppData\\Local\\Jenkins.jenkins\\workspace\\JenkinsLabUser1\\SimpleChecksum\\temp\\test.txt");
		 
		//Use MD5 algorithm
		MessageDigest md5Digest = null;
		try {
			md5Digest = MessageDigest.getInstance("SHA");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		//Get the checksum
		String checksum = null;
		try {
			checksum = Checksum.getFileChecksum(md5Digest, file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		AssertJUnit.assertEquals(checksum, "cb78b9f5127f48a2a8d2a6ce56ec5bae");
		
	}

	@Test
	public void testFile2() {
		//Create checksum for this file
		File file = new File("C:\\Users\\Administrator\\AppData\\Local\\Jenkins.jenkins\\workspace\\JenkinsLabUser1\\SimpleChecksum\\temp\\test2.txt");	
		
		//Use MD5 algorithm
		MessageDigest md5Digest = null;
		try {
			md5Digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		//Get the checksum
		String checksum = null;
		try {
			checksum = Checksum.getFileChecksum(md5Digest, file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
		AssertJUnit.assertEquals(checksum, "d6ba571a2c372256e38670d0eea7e4f8");
	}
}
